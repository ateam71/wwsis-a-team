package com.example.punch.krokomierz;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Chronometer;
import android.app.Activity;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity implements OnClickListener,SensorEventListener {
    private Chronometer chronometer;
    private long timeDifference = 0;

    private SensorManager senSensorManager;
    private Sensor senAccelerometer;

    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    private static final int SHAKE_THRESHOLD = 550;

    int i;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        chronometer = (Chronometer) findViewById(R.id.chronometer);
        ((Button) findViewById(R.id.start_button)).setOnClickListener(this);
        ((Button) findViewById(R.id.stop_button)).setOnClickListener(this);
        ((Button) findViewById(R.id.pause_button)).setOnClickListener(this);
        ((Button) findViewById(R.id.resume_button)).setOnClickListener(this);

        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;
        TextView licznik = (TextView) findViewById(R.id.licznik);


        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            long curTime = System.currentTimeMillis();

            if ((curTime - lastUpdate) > 100) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                float speed = Math.abs(x + y + z - last_x - last_y - last_z)/ diffTime * 10000;

                if (speed > SHAKE_THRESHOLD) {
                    i++;
                    licznik.setText(String.valueOf(i));
                }

                last_x = x;
                last_y = y;
                last_z = z;


            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }


    @Override
    public void onClick(View v) {

        TextView licznik = (TextView) findViewById(R.id.licznik);
        EditText waga = (EditText) findViewById(R.id.waga);
        EditText wzrost = (EditText) findViewById(R.id.wzrost);
        TextView odleglosc = (TextView) findViewById(R.id.odleglosc);
        TextView kalorie = (TextView) findViewById(R.id.kalorie);

        switch(v.getId()) {
            case R.id.start_button:
                timeDifference = 0;
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
                senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

                break;
            case R.id.stop_button:
                chronometer.stop();
                senSensorManager.unregisterListener(this);

                float x = Integer.parseInt(wzrost.getText().toString());
                float y = Integer.parseInt(licznik.getText().toString());
                odleglosc.setText(String.valueOf(Math.floor(x/100*y*0.45)));

                //--------------------------------------------------------------------------------------------------

                //.03 x wt in lb
                timeDifference  = chronometer.getBase() - SystemClock.elapsedRealtime();

                x = Integer.parseInt(waga.getText().toString());
                y = SystemClock.elapsedRealtime() - chronometer.getBase();;

                String cal = String.valueOf(x * 2.20462262 * 0.03 * y / 60000);
                cal = cal.substring(0, cal.length() - 10);
                        kalorie.setText(cal);

                break;
            case R.id.pause_button:


                timeDifference  = chronometer.getBase() - SystemClock.elapsedRealtime();
                chronometer.stop();
                senSensorManager.unregisterListener(this);
                break;
            case R.id.resume_button:
                chronometer.setBase(SystemClock.elapsedRealtime() + timeDifference);
                chronometer.start();
                senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
                break;
        }
    }
}